function x = cosamp(phi, y, s)
  ## Entradas:
  ##   A: Matriz de medicion
  ##   y: Vector ruidoso de entrada
  ##   s: Esparcidad
  ## Parametros Iniciales:
  ##   r_0: Error recidual
  ##   x_0: Estimado inicial
  ##   T: Conjunto de columnas para conformar la esparcidad de x
  ## Salidas:
  ##   x: Vector estimado

  TOL = 1e-3;
  x = zeros(size(phi)(2), size(y)(2));
  v = y;
  k = 0;

  for i=1:100
    k = k + 1;

    z = phi' * v;
    z = threshold(z, 1e-12);

    Omega = supp(H_k(z, 2 * s));

    Gamma =  union(Omega, supp(x));

    x_bar = pinv(k_columns(phi, Gamma)) * y;
    x_bar = threshold(x_bar, 1e-12);

    oldx = x;

    x = H_k(x_bar, s);

    v = y - phi * x;

    if norm(x - oldx) < TOL
      break;
    endif

  endfor

endfunction
