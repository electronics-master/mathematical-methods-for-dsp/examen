function sx = sparse_x (n, s)
  sx = full(sprand(n, 1, s/n));
endfunction
