function x = mfr(A, y, k, mu) 
  ## Entradas:
  ##   A: Matriz de medicion
  ##   y: Vector ruidoso de entrada
  ##   k: Esparcidad
  ##   mu: Tamanho de el paso
  ## Parametros Iniciales:
  ##   x_0: Estimado inicial
  ## Salidas:
  ##   x: Vector estimado

  m = size(A)(2);
  TOL = 1e-3;

  x = zeros(m, 1);

  for i=1:1000
    oldx = x;
    x = H_k(x + mu * A' * (y - A * x), k);

    if norm(x - oldx) < TOL
      break;
    endif
  endfor
endfunction
