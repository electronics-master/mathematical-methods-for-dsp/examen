function T = supp(x)
  ## Return the support of x
  ## This is the position of the non-zero components of x

  T = find(x);
endfunction
