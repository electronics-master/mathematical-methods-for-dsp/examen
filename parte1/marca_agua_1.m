pkg load signal; %% Used for DCT
pkg load image; %% Used to read and write images


W = double(imread("marca.jpg"));
I = double(imread("imagen1.jpg"));

alpha = 0.1;

n = 8; %% Block size

disp("Starting encoding")

F = zeros(size(I));
A = zeros(size(I)/n);

for i=1:size(A)(1)
  for j=1:size(A)(2)
    block_ij = dct2(I( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ));

    F( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ) = block_ij;

    A(i, j) = block_ij(1, 1);
  endfor
endfor

[U, S, V] = svd(A);

[U_1, S_1, V_1] = svd (alpha * W + S);

A_hat = U * S_1 * V';

I_wm = zeros(size(I));

for i=1:size(A_hat)(1)
  for j=1:size(A_hat)(2)
    F( n*(i-1)+1, n*(j-1)+1) = A_hat(i, j);
        
    I_wm ( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ) = idct2(F( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ));
  endfor
endfor

disp("Finished encoding")

%% Decompressing

disp("Starting decoding")

A_wm = zeros(size(I)/n);

for i=1:size(A)(1)
  for j=1:size(A)(2)
    block_ij = dct2(I_wm( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ));

    A_wm(i, j) = block_ij(1, 1);
  endfor
endfor

[U, S_1, V] = svd(A_wm);

D = U_1  * S_1 * V_1';

W_wm = 1 / alpha * (D - S);

disp("Finished decoding")


hf = figure(1, 'position',get(0,"screensize"));

subplot(2,2,1)
imshow(I, [0, 255])
title("Imagen Original")

subplot(2,2,2)
imshow(W, [0, 255])
title("Marca de Agua")

subplot(2,2,3)
imshow(I_wm, [0, 255])
title("Imagen con Marca de Agua")

subplot(2,2,4)
imshow(W_wm, [0, 255])
title("Marca de Agua Extraida")

drawnow;
  
print(hf, "marca_de_agua_1", "-dpdflatexstandalone")

input("Press any key to exit");
