pkg load signal; %% Used for DCT
pkg load image; %% Used to read and write images

I_wm = imread("imagen2.jpg");

alpha = 0.1;

load("U1.mat");

load("V1.mat");

n = 4;

%% Decompressing

A_wm = zeros(size(I_wm)/n);

for i=1:size(A_wm)(1)
  for j=1:size(A_wm)(2)
    block_ij = dct2(I_wm( n*(i-1)+1 : n*i, n*(j-1)+1:n*j ));

    A_wm(i, j) = block_ij(1, 1);
  endfor
endfor

[U, S_1, V] = svd(A_wm);

D = U1  * S_1 * V1';

W = 1 / alpha * (D);

hf = figure(1, 'position',get(0,"screensize"));

subplot(2,1,1)
imshow(I_wm, [0, 255])
title("Imagen Original con Ruido")

subplot(2,1,2)
imshow(W, [0, 255])
title("Marca de Agua Extraida")

drawnow;

input("Press any key to exit");
  
print(hf, "marca_de_agua_2", "-dpdflatexstandalone")
