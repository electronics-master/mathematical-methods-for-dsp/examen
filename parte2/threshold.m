function x = threshold (x, tol)
    indices = find(abs(x)<tol);
    x(indices) = 0;
endfunction
