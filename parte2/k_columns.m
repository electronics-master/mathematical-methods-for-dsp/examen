function x_k = k_columns(x, K)
  x_k = zeros(size(x));

  K = sort(K);
  
  for i=K
    x_k(:, i) = x(:, i);
  endfor
  
endfunction
