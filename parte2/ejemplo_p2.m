mu = 1;

ITERATIONS = 100

ns = [400, 400, 400, 400, 800, 800, 800, 800];
ms =  [50, 100, 150, 200, 100, 200, 300, 400];
min_spar = [1,  10, 20, 60, 1, 30, 50, 130 ];
max_spar = [16, 40, 65, 90, 30, 70, 120, 170];


tb = [ns; ms; min_spar; max_spar];

for i=1:size(tb)(2)

  n = tb(1, i);
  m = tb(2, i);
  min_sparcity = tb(3, i);
  max_sparcity = tb(4, i);
  
  cosamp_success = zeros(max_sparcity - min_sparcity + 1, 1);
  mfr_success = zeros(max_sparcity - min_sparcity + 1, 1);

  for s=min_sparcity:max_sparcity
    s
    for i=1:ITERATIONS
      
      ## Measurement matrix
      Phi = (1/m).*randn(m, n);


      ## Take step according to 6.8
      S = svd(Phi' * Phi);
      mu = 2 / (S(1) + S(size(S)(1)) );

      ## Target x
      x = sparse_x(n, s);

      ##
      y = Phi * x;

      ##
      x_cosamp = cosamp(Phi, y, s);
      x_mfr = mfr(Phi, y, s, mu);


      ## if(sum(supp(x) == supp(x_cosamp)) == nnz(x))
      ##   cosamp_success(s) += 1;
      ## endif

      ## if(sum(supp(x) == supp(x_mfr)) == nnz(x))
      ##   mfr_success(s) += 1;
      ## endif

      ## [supp(x), supp(x_cosamp), supp(x_mfr)]

      cosamp_success(s  - min_sparcity + 1) += (sum(supp(x) == supp(x_cosamp))/nnz(x));

      mfr_success(s  - min_sparcity + 1) += (sum(supp(x) == supp(x_mfr))/nnz(x));
    endfor
  endfor
  cosamp_success = cosamp_success ./ ITERATIONS .* 100;
  mfr_success = mfr_success ./ ITERATIONS .* 100;

  h = figure(1);
  plot(min_sparcity:max_sparcity, horzcat(cosamp_success, mfr_success));
  legend ({"CoSaMP", "MFR"}, "location", "NorthEastOutside");
  ylabel("Tasa de Éxito de Reconstrucción (%)");
  xlabel("Esparcidad");
  title(sprintf("N = %d, M = %d", n, m));
  
  print(h, sprintf("%dx%d", n, m), "-dpdflatexstandalone")

  drawnow;

  ##input("Press any key to continue");
  
endfor
